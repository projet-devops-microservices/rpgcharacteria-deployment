install:
	ansible-galaxy install -r requirements.yml

deploy:
	ansible-playbook -i inventory.yml system.yml --ask-vault-pass

deploy-runner:
	ansible-playbook -i inventory.yml system.yml --ask-vault-pass --tags runner