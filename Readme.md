# Automatisation de l'infrastructure

## Installation de l'environnement

### 1. Créer une VM sous debian et installer (openssh-server si ce n'est pas déjà fait)
Installer openssh-server :
```
apt install openssh-server -y
```
>Ce paquet permet de créer un serveur ssh sur sa machine (secure shell),le port par défaut est 22, puis si ce n'est pas fais démarrer le service
    service sshd start

### 2. Installer ansible depuis la wsl
Installer ansible: 
```
apt install ansible -y
```
>Ce paquet nous permettra de créer automatiquement le même environnement sur les serveurs que l'on veuut.

### 3. Générer une paire de clé avec ssh-keygen depuis la wsl
Exécuter la commande :
```
ssh-keygen
```
>La clé publique est un fichier terminant par .pub et se trouve si la commande est éxécuté avec la wsl dans /home/users/.ssh

### 4. Ajouter la clé publique à notre serveur distant pour s'y connecter en SSH
    ssh-copy-id remote_user@remote_ip
>Cette commande permet de se connecter en ssh avec l'utilisateur défini en évitant de rentrer le mot de passe à chaque connexion

## Configuration Ansible

### 1. Créer un fichier inventory.yml dans le répertoire de notre projet, indiquant tous les serveurs qui seront connectés à ansible
    webservers:
        vars:
            ansible_user : coding
        hosts:
    coding_massi:
        ansible_host : 10.160.32.213
        runner_tags:
            - prod
    coding_widad:
        ansible_host : 10.160.33.153
    coding_victor:
        ansible_host : 10.160.24.97
    coding_rihab:
        ansible_host : 10.160.32.91
> Le runner tags sert à définir des labels pour certains serveur par exemple la prod, lorsqu'on veut effectué un job sur le serveur prod.
> La variable "ansible_user: coding" sert à éviter du duplicata de code.
> La propriété "ansible_host" demande l'adresse du serveur distant pour créer un lien avec ansible

### 2. Vérifier si ansible arrive à établir une connexion
    ansible -i inventory.yml all -m ping
>Affiche les facts (Description du serveur distant) tout en faisant un ping qui renvoie pong si la connexion est établie.

### 3. Déployer un nouvel administrateur

Pour déployer un nouvel administrateur il faut d'abord créer un playbook.

## Création du playbook
Créer un fichier nommé system.yml :
```
- hosts: all
  become: true
  gather_facts: true
  
    tasks:
    - name: Create user
      user:
        name: esiee
        state: present
```

Maintenant on va lui rajouter les clés publique généré avant pour permettre une connexion ssh sans demande de mot de passe.
On l'ajoute ensuite au groupe sudo.

```
- hosts: all
  become: true
  gather_facts: true
  
    tasks:
    - name: Create user
      user:
        name: esiee
        state: present
    - name: Set up multiple authorized keys
      ansible.posix.authorized_key:
        user: esiee
        state: present
        key: '{{ item }}'
      with_file:
        - public_keys/massimiliano_keys.pub
        - public_keys/widad_keys.pub
        - public_keys/rihab_keys.pub
        - public_keys/victor_keys.pub

    - name: Ensure esiee has passwordless sudo capabilities
      lineinfile:
        path: /etc/sudoers.d/esiee
        line: 'esiee ALL=(ALL) NOPASSWD: ALL'
        create: yes
        validate: '/usr/sbin/visudo -cf %s'
```
